// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package stream2bq

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

func TestIntegPersistAsset(t *testing.T) {
	testCases := []struct {
		name            string
		path            string
		wantMsgContains []string
	}{
		{
			name: "asset01_rces",
			path: "testdata/assetFeed_01.json",
		},
		{
			name: "asset02_iam",
			path: "testdata/assetFeed_02.json",
		},
	}
	var step glo.Step
	step.StepID = "//pubsub.googleapis.com/projects/qwerty-ram-qa-100/topics/actionTrigger/5588803362056255"

	var ev glo.EntryValues
	ev.StepStack = make(glo.Steps, 0)
	ev.StepStack = append(ev.StepStack, step)

	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			p := filepath.Clean(tc.path)
			if !strings.HasPrefix(p, "testdata/") {
				panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
			}
			b, err := os.ReadFile(p)
			if err != nil {
				log.Fatalln(err)
			}
			var feedMsg cai.FeedMessage
			err = json.Unmarshal(b, &feedMsg)
			if err != nil {
				log.Fatalln(err)
			}

			err = persistAsset(ctx, &feedMsg, global.inserterAssets, ev)
			if err != nil {
				t.Errorf("want no error and got %v", err)
			}
		})
	}
}
