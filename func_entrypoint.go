// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//..

package stream2bq

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"cloud.google.com/go/pubsub"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

// EntryPoint function entry point.
func EntryPoint(ctxEvent context.Context, pubsubMsg pubsub.Message) error {
	b, _ := json.MarshalIndent(pubsubMsg, "", "  ")

	var ev glo.EntryValues
	ev.CommonEntryValues = global.CommonEv

	msgTimestamp, err := time.Parse(time.RFC3339, pubsubMsg.Attributes["timestamp"])
	if err != nil {
		glo.LogCriticalNoRetry(ev, "triggering PubSub message attribute 'timestamp' is missing or invalid and should not", "", "")
		return nil
	}

	now := time.Now()
	d := now.Sub(msgTimestamp)

	glo.LogStartCloudEvent(ev, b, d.Seconds(), &now)

	if pubsubMsg.Attributes["origin"] == "" {
		glo.LogCriticalNoRetry(ev, "triggering PubSub message attribute 'origin' is zero value and should not", "", "")
		return nil
	}

	if pubsubMsg.Attributes["assetType"] == "" {
		glo.LogCriticalNoRetry(ev, "triggering PubSub message attribute 'assetType' is zero value and should not", "", "")
		return nil
	}

	if pubsubMsg.Attributes["contentType"] == "" {
		glo.LogCriticalNoRetry(ev, "triggering PubSub message attribute 'contentType' is zero value and should not", "", "")
		return nil
	}

	ev.Step = glo.Step{
		StepID:        fmt.Sprintf("%s/%s/%s/%s/%s", pubsubMsg.Attributes["messageType"], pubsubMsg.Attributes["origin"], pubsubMsg.Attributes["assetType"], pubsubMsg.Attributes["contentType"], pubsubMsg.ID),
		StepTimestamp: msgTimestamp,
	}
	ev.StepStack = make(glo.Steps, 0)
	ev.StepStack = append(ev.StepStack, ev.Step)

	finishMsg := ""
	finishDescription := ""
	assetInventoryOrigin := ""
	assetType := ""
	contentType := ""
	ruleName := ""
	var messageType string

	if strings.Contains(pubsubMsg.Attributes["messageType"], "violation_on_rule") {
		messageType = "violation_on_rule"
	} else {
		messageType = pubsubMsg.Attributes["messageType"]
	}

	switch messageType {
	case "asset_feed":
		var feedMessage cai.FeedMessage
		err := json.Unmarshal(pubsubMsg.Data, &feedMessage)
		if err != nil {
			glo.LogCriticalNoRetry(ev, fmt.Sprintf("json.Unmarshal(b, &feedMessage) %v", err), "", "")
			return nil
		}
		assetType = feedMessage.Asset.AssetType
		contentType = feedMessage.ContentType
		ev.StepStack = append(feedMessage.StepStack, ev.Step)
		err = persistAsset(ctxEvent, &feedMessage, global.inserterAssets, ev)
		if err != nil {
			glo.LogCriticalRetry(ev, fmt.Sprintf("persistAsset(&feedMessage) %v", err), assetType, "")
			return err
		}
		finishMsg = "asset persisted"
		finishDescription = fmt.Sprintf("%s %s timestamp %v", feedMessage.Asset.Name, feedMessage.Asset.AncestryPathDisplayName, feedMessage.Window.StartTime)
		assetInventoryOrigin = feedMessage.Origin
	case "compliance_status":
		var complianceStatus cai.ComplianceStatus
		err := json.Unmarshal(pubsubMsg.Data, &complianceStatus)
		if err != nil {
			glo.LogCriticalNoRetry(ev, fmt.Sprintf("json.Unmarshal(b, &complianceStatus) %v", err), "", "")
			return nil
		}
		assetType = complianceStatus.AssetType
		ruleName = complianceStatus.RuleName
		ev.StepStack = append(complianceStatus.StepStack, ev.Step)
		err = persistComplianceStatus(ctxEvent, &complianceStatus, global.inserterComplianceStatus, ev)
		if err != nil {
			glo.LogCriticalRetry(ev, fmt.Sprintf("persistCompliancestatus(&complianceStatus) %v", err), assetType, ruleName)
			return err
		}
		finishMsg = "compliance status persisted"
		finishDescription = fmt.Sprintf("%s %s evaluation timestamp %v", complianceStatus.AssetName, complianceStatus.RuleName, complianceStatus.EvaluationTimeStamp)
		assetInventoryOrigin = complianceStatus.AssetInventoryOrigin
	case "violation_on_rule":
		var violation violation
		err := json.Unmarshal(pubsubMsg.Data, &violation)
		if err != nil {
			glo.LogCriticalNoRetry(ev, fmt.Sprintf("json.Unmarshal(b, &violation) %v", err), "", "")
			return nil
		}
		assetType = violation.FeedMessage.Asset.AssetType
		contentType = violation.FeedMessage.ContentType
		ruleName = violation.FunctionConfig.FunctionName
		ev.StepStack = append(violation.StepStack, ev.Step)
		err = persistViolation(ctxEvent, &violation, global.inserterViolations, ev)
		if err != nil {
			glo.LogCriticalRetry(ev, fmt.Sprintf("persistViolation(&violation) %v", err), assetType, ruleName)
			return err
		}
		finishMsg = "violation persisted"
		finishDescription = fmt.Sprintf("%s %s evaluation timestamp %v", violation.NonCompliance.Message, violation.FeedMessage.Asset.Name, violation.NonCompliance.EvaluationTimeStamp)
		assetInventoryOrigin = violation.FeedMessage.Origin
	default:
		glo.LogCriticalNoRetry(ev, fmt.Sprintf("unmanaged message type %s", pubsubMsg.Attributes["messageType"]), "", "")
		return nil
	}

	glo.LogFinish(ev, finishMsg, finishDescription, time.Now(), assetInventoryOrigin, assetType, contentType, ruleName, 0)
	return nil
}
