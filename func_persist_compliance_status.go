// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package stream2bq

import (
	"context"
	"fmt"

	"cloud.google.com/go/bigquery"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
	"gitlab.com/realtime-asset-monitor/utilities/str"
)

func persistComplianceStatus(ctx context.Context, complianceStatus *cai.ComplianceStatus,
	inserter *bigquery.Inserter,
	ev glo.EntryValues) (err error) {
	complianceStatusSchema := bigquery.Schema{
		{Name: "assetName", Required: true, Type: bigquery.StringFieldType},
		{Name: "assetInventoryTimeStamp", Required: true, Type: bigquery.TimestampFieldType, Description: "When the asset change was captured"},
		{Name: "assetInventoryOrigin", Required: false, Type: bigquery.StringFieldType, Description: "Mean to capture the asset change: real-time or batch-export"},
		{Name: "ruleName", Required: true, Type: bigquery.StringFieldType},
		{Name: "ruleDeploymentTimeStamp", Required: true, Type: bigquery.TimestampFieldType, Description: "When the rule was assessed"},
		{Name: "compliant", Required: true, Type: bigquery.BooleanFieldType},
		{Name: "deleted", Required: true, Type: bigquery.BooleanFieldType},
		{Name: "evaluationTimeStamp", Required: true, Type: bigquery.TimestampFieldType, Description: "When the compliance rule was evaluated on the asset settings"},
		{Name: "assetType", Required: false, Type: bigquery.StringFieldType},
		{Name: "scheduledRootTriggeringID", Required: false, Type: bigquery.StringFieldType},
		{Name: "scheduledRootTriggeringTimestamp", Required: false, Type: bigquery.TimestampFieldType, Description: "When the triggering scheduled export was launched"},
	}
	var complianceStatusBQ complianceStatusBQ
	complianceStatusBQ.AssetName = complianceStatus.AssetName
	complianceStatusBQ.AssetInventoryTimeStamp = complianceStatus.AssetInventoryTimeStamp
	complianceStatusBQ.AssetInventoryOrigin = complianceStatus.AssetInventoryOrigin
	complianceStatusBQ.RuleName = complianceStatus.RuleName
	complianceStatusBQ.RuleDeploymentTimeStamp = complianceStatus.RuleDeploymentTimeStamp
	complianceStatusBQ.Compliant = complianceStatus.Compliant
	complianceStatusBQ.Deleted = complianceStatus.Deleted
	complianceStatusBQ.EvaluationTimeStamp = complianceStatus.EvaluationTimeStamp
	complianceStatusBQ.AssetType = complianceStatus.AssetType
	if complianceStatus.AssetInventoryOrigin != "real-time" {
		complianceStatusBQ.ScheduledRootTriggeringID = ev.StepStack[0].StepID
		complianceStatusBQ.ScheduledRootTriggeringTimestamp = ev.StepStack[0].StepTimestamp
	}

	insertID := str.Hash(fmt.Sprintf("%s%v%s%v%v",
		complianceStatus.AssetName,
		complianceStatus.AssetInventoryTimeStamp,
		complianceStatus.RuleName,
		complianceStatus.RuleDeploymentTimeStamp,
		complianceStatus.EvaluationTimeStamp))
	savers := []*bigquery.StructSaver{
		{
			Struct:   complianceStatusBQ,
			Schema:   complianceStatusSchema,
			InsertID: insertID,
		},
	}
	err = inserter.Put(ctx, savers)
	return err
}
