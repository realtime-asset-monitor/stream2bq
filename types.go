// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package stream2bq

import (
	"encoding/json"
	"time"

	"cloud.google.com/go/bigquery"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

const microserviceName = "stream2bq"

// ServiceEnv list environment variables prefixed with the name of the microservice
type ServiceEnv struct {
	Environment               string `envconfig:"environment" default:"dev"`
	DataSetName               string `envconfig:"dataset_name" default:"ram"`
	LogOnlySeveritylevels     string `envconfig:"log_only_severity_levels" default:"WARNING NOTICE CRITICAL"`
	ProjectID                 string `envconfig:"project_id" required:"true"`
	StartProfiler             bool   `envconfig:"start_profiler" default:"false"`
	TableNameAssets           string `envconfig:"table_name_assets" default:"assets"`
	TableNameComplianceStatus string `envconfig:"table_name_assets" default:"complianceStatus"`
	TableNameViolations       string `envconfig:"table_name_assets" default:"violations"`
}

// Env list environment variables
type Env struct {
	KConfiguration string `envconfig:"k_configuration"`
	KRevision      string `envconfig:"k_revision"`
	KService       string `envconfig:"k_service"`
}

// Global structure for global variables to optimize the performances in serverless mode
type Global struct {
	CommonEv                 glo.CommonEntryValues
	env                      *Env
	inserterAssets           *bigquery.Inserter
	inserterComplianceStatus *bigquery.Inserter
	inserterViolations       *bigquery.Inserter
	serviceEnv               *ServiceEnv
}

// violation from the "audit" rego policy in "audit.rego" module
type violation struct {
	NonCompliance    nonCompliance    `json:"nonCompliance"`
	FunctionConfig   functionConfig   `json:"functionConfig"`
	ConstraintConfig constraintConfig `json:"constraintConfig"`
	FeedMessage      feedMessage      `json:"feedMessage"`
	RegoModules      json.RawMessage  `json:"regoModules"`
	StepStack        glo.Steps        `json:"step_stack,omitempty"`
}

// violationBQ from the "audit" rego policy in "audit.rego" module
type violationBQ struct {
	NonCompliance    nonComplianceBQ    `json:"nonCompliance"`
	FunctionConfig   functionConfig     `json:"functionConfig"`
	ConstraintConfig constraintConfigBQ `json:"constraintConfig"`
	FeedMessage      feedMessageBQ      `json:"feedMessage"`
	RegoModules      string             `json:"regoModules"`
}

// nonCompliance form the "deny" rego policy in a <templateName>.rego module
type nonCompliance struct {
	EvaluationTimeStamp time.Time       `json:"evaluationTimeStamp"`
	Message             string          `json:"message"`
	Metadata            json.RawMessage `json:"metadata"`
}

// nonComplianceBQ form the "deny" rego policy in a <templateName>.rego module
type nonComplianceBQ struct {
	EvaluationTimeStamp time.Time `json:"evaluationTimeStamp"`
	Message             string    `json:"message"`
	Metadata            string    `json:"metadata"`
}

// functionConfig function deployment settings
type functionConfig struct {
	FunctionName   string    `json:"functionName"`
	DeploymentTime time.Time `json:"deploymentTime"`
	ProjectID      string    `json:"projectID"`
	Environment    string    `json:"environment"`
}

// constraintConfig expose content of the constraint yaml file
type constraintConfig struct {
	APIVersion string             `json:"apiVersion"`
	Kind       string             `json:"kind"`
	Metadata   constraintMetadata `json:"metadata"`
	Spec       spec               `json:"spec"`
}

// constraintConfigBQ format to persist in BQ
type constraintConfigBQ struct {
	Kind     string               `json:"kind"`
	Metadata constraintMetadataBQ `json:"metadata"`
	Spec     specBQ               `json:"spec"`
}

// constraintMetadata Constraint's metadata
type constraintMetadata struct {
	Name        string          `json:"name"`
	Annotations json.RawMessage `json:"annotations"`
}

// constraintMetadataBQ format to persist in BQ
type constraintMetadataBQ struct {
	Name        string `json:"name"`
	Annotations string `json:"annotations"`
}

// spec Constraint's specifications
type spec struct {
	Severity   string          `json:"severity"`
	Match      json.RawMessage `json:"match"`
	Parameters json.RawMessage `json:"parameters"`
}

// specBQ format to persist in BQ
type specBQ struct {
	Severity   string `json:"severity"`
	Match      string `json:"match"`
	Parameters string `json:"parameters"`
}

// feedMessage Cloud Asset Inventory feed message
type feedMessage struct {
	Asset       asset      `json:"asset"`
	ContentType string     `json:"contentType,omitempty"`
	Window      cai.Window `json:"window"`
	Origin      string     `json:"origin"`
	StepStack   glo.Steps  `json:"step_stack,omitempty"`
}

// feedMessageBQ format to persist in BQ
type feedMessageBQ struct {
	Asset  assetBQ    `json:"asset"`
	Window cai.Window `json:"window"`
	Origin string     `json:"origin"`
}

// asset Cloud Asset Metadata
type asset struct {
	Name                    string          `json:"name"`
	Owner                   string          `json:"owner"`
	ViolationResolver       string          `json:"violationResolver"`
	AncestryPathDisplayName string          `json:"ancestryPathDisplayName"`
	AncestryPath            string          `json:"ancestryPath"`
	AncestorsDisplayName    json.RawMessage `json:"ancestorsDisplayName"`
	Ancestors               json.RawMessage `json:"ancestors"`
	AssetType               string          `json:"assetType"`
	IamPolicy               json.RawMessage `json:"iamPolicy"`
	Resource                json.RawMessage `json:"resource"`
	UpdateTime              time.Time       `json:"update_time"`
}

// assetBQ format to persist asset in BQ violations table
type assetBQ struct {
	Name                      string    `json:"name"`
	Owner                     string    `json:"owner"`
	ViolationResolver         string    `json:"violationResolver"`
	AncestryPathDisplayName   string    `json:"ancestryPathDisplayName"`
	AncestryPath              string    `json:"ancestryPath"`
	AncestorsDisplayName      string    `json:"ancestorsDisplayName"`
	Ancestors                 string    `json:"ancestors"`
	AssetType                 string    `json:"assetType"`
	IamPolicy                 string    `json:"iamPolicy"`
	Resource                  string    `json:"resource"`
	UpdateTime                time.Time `json:"update_time"`
	ScheduledRootTriggeringID string    `json:"scheduledRootTriggeringID"`
}

// assetAssetBQ format to persist asset in BQ assets table
type assetAssetBQ struct {
	Ancestors                 []string  `json:"ancestors"`
	AncestorsDisplayName      []string  `json:"ancestorsDisplayName"`
	AncestryPath              string    `json:"ancestryPath"`
	AncestryPathDisplayName   string    `json:"ancestryPathDisplayName"`
	AssetType                 string    `json:"assetType"`
	Deleted                   bool      `json:"deleted"`
	Name                      string    `json:"name"`
	Owner                     string    `json:"owner"`
	ProjectID                 string    `json:"projectID"`
	Timestamp                 time.Time `json:"timestamp"`
	ViolationResolver         string    `json:"violationResolver"`
	PersistTimestamp          time.Time `json:"persistTimeStamp"`
	ScheduledRootTriggeringID string    `json:"scheduledRootTriggeringID"`
}

// complianceStatusBQ to persist compliance status in complianceStatus table
type complianceStatusBQ struct {
	AssetInventoryOrigin             string    `json:"assetInventoryOrigin"`
	AssetInventoryTimeStamp          time.Time `json:"assetInventoryTimeStamp"`
	AssetName                        string    `json:"assetName"`
	AssetType                        string    `json:"assetType,omitempty"`
	Compliant                        bool      `json:"compliant"`
	Deleted                          bool      `json:"deleted"`
	EvaluationTimeStamp              time.Time `json:"evaluationTimeStamp"`
	RuleDeploymentTimeStamp          time.Time `json:"ruleDeploymentTimeStamp"`
	RuleName                         string    `json:"ruleName"`
	ScheduledRootTriggeringID        string    `json:"scheduledRootTriggeringID"`
	ScheduledRootTriggeringTimestamp time.Time `json:"scheduledRootTriggeringTimestamp"`
}
