// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package stream2bq

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

func TestIntegPersistViolation(t *testing.T) {
	testCases := []struct {
		name            string
		path            string
		wantMsgContains []string
	}{
		{
			name: "violation01",
			path: "testdata/violation_01.json",
		},
		{
			name: "violation02",
			path: "testdata/violation_02.json",
		},
	}
	var step glo.Step
	step.StepID = "//pubsub.googleapis.com/projects/qwerty-ram-qa-100/topics/actionTrigger/5588803362056255"
	var ev glo.EntryValues
	ev.StepStack = make(glo.Steps, 0)
	ev.StepStack = append(ev.StepStack, step)

	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			p := filepath.Clean(tc.path)
			if !strings.HasPrefix(p, "testdata/") {
				panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
			}
			b, err := os.ReadFile(p)
			if err != nil {
				log.Fatalln(err)
			}
			var violation violation
			err = json.Unmarshal(b, &violation)
			if err != nil {
				log.Fatalln(err)
			}

			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()

			err = persistViolation(ctx, &violation, global.inserterViolations, ev)
			msgString := buffer.String()
			t.Logf("%s", msgString)

			if err != nil {
				t.Errorf("want no error and got %v", err)
			}
		})
	}
}
