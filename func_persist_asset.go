// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package stream2bq

import (
	"context"
	"fmt"
	"time"

	"cloud.google.com/go/bigquery"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
	"gitlab.com/realtime-asset-monitor/utilities/str"
)

func persistAsset(ctx context.Context,
	feedMessage *cai.FeedMessage,
	inserter *bigquery.Inserter,
	ev glo.EntryValues) (err error) {
	assetsSchema := bigquery.Schema{
		{Name: "timestamp", Required: true, Type: bigquery.TimestampFieldType},
		{Name: "name", Required: true, Type: bigquery.StringFieldType},
		{Name: "owner", Required: false, Type: bigquery.StringFieldType},
		{Name: "violationResolver", Required: false, Type: bigquery.StringFieldType},
		{Name: "ancestryPathDisplayName", Required: false, Type: bigquery.StringFieldType},
		{Name: "ancestryPath", Required: false, Type: bigquery.StringFieldType},
		{Name: "ancestorsDisplayName", Repeated: true, Type: bigquery.StringFieldType},
		{Name: "ancestors", Repeated: true, Type: bigquery.StringFieldType},
		{Name: "assetType", Required: true, Type: bigquery.StringFieldType},
		{Name: "deleted", Required: true, Type: bigquery.BooleanFieldType},
		{Name: "projectID", Required: false, Type: bigquery.StringFieldType},
		{Name: "persistTimestamp", Required: false, Type: bigquery.TimestampFieldType},
		{Name: "scheduledRootTriggeringID", Required: false, Type: bigquery.StringFieldType},
	}

	var assetBQ assetAssetBQ
	assetBQ.Ancestors = feedMessage.Asset.Ancestors
	assetBQ.AncestorsDisplayName = feedMessage.Asset.AncestorsDisplayName
	assetBQ.AncestryPath = feedMessage.Asset.AncestryPath
	assetBQ.AncestryPathDisplayName = feedMessage.Asset.AncestryPathDisplayName
	assetBQ.AssetType = feedMessage.Asset.AssetType
	assetBQ.Deleted = feedMessage.Deleted
	assetBQ.Name = feedMessage.Asset.Name
	assetBQ.Owner = feedMessage.Asset.Owner
	assetBQ.ProjectID = feedMessage.Asset.ProjectID
	assetBQ.ViolationResolver = feedMessage.Asset.ViolationResolver
	assetBQ.PersistTimestamp = time.Now()

	if feedMessage.Origin == "real-time" {
		assetBQ.Timestamp = feedMessage.Window.StartTime
	} else {
		assetBQ.Timestamp = feedMessage.Asset.UpdateTime
		assetBQ.ScheduledRootTriggeringID = ev.StepStack[0].StepID
	}

	insertID := str.Hash(fmt.Sprintf("%s%v%v",
		assetBQ.Name,
		assetBQ.Timestamp,
		assetBQ.PersistTimestamp))
	savers := []*bigquery.StructSaver{
		{
			Struct:   assetBQ,
			Schema:   assetsSchema,
			InsertID: insertID,
		},
	}
	err = inserter.Put(ctx, savers)
	return err
}
