// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package stream2bq

import (
	"context"
	"fmt"

	"cloud.google.com/go/bigquery"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
	"gitlab.com/realtime-asset-monitor/utilities/str"
)

func persistViolation(ctx context.Context,
	violation *violation,
	inserter *bigquery.Inserter,
	ev glo.EntryValues) (err error) {

	violationSchema := bigquery.Schema{
		{
			Name:        "nonCompliance",
			Type:        bigquery.RecordFieldType,
			Description: "The violation information, aka why it is not compliant",
			Schema: bigquery.Schema{
				{Name: "evaluationTimeStamp", Required: true, Type: bigquery.TimestampFieldType, Description: "When the compliance rule was evaluated on the asset settings"},
				{Name: "message", Required: true, Type: bigquery.StringFieldType},
				{Name: "metadata", Required: false, Type: bigquery.StringFieldType},
			},
		},
		{
			Name:        "functionConfig",
			Type:        bigquery.RecordFieldType,
			Description: "The settings of the cloud function hosting the rule check",
			Schema: bigquery.Schema{
				{Name: "functionName", Required: true, Type: bigquery.StringFieldType},
				{Name: "deploymentTime", Required: true, Type: bigquery.TimestampFieldType},
				{Name: "projectID", Required: false, Type: bigquery.StringFieldType},
				{Name: "environment", Required: false, Type: bigquery.StringFieldType},
			},
		},
		{
			Name:        "constraintConfig",
			Type:        bigquery.RecordFieldType,
			Description: "The settings of the constraint used in conjunction with the rego template to assess the rule",
			Schema: bigquery.Schema{
				{Name: "kind", Required: false, Type: bigquery.StringFieldType},
				{
					Name: "metadata",
					Type: bigquery.RecordFieldType,
					Schema: bigquery.Schema{
						{Name: "name", Required: false, Type: bigquery.StringFieldType},
						{Name: "annotations", Required: false, Type: bigquery.StringFieldType},
					},
				},
				{
					Name: "spec",
					Type: bigquery.RecordFieldType,
					Schema: bigquery.Schema{
						{Name: "severity", Required: false, Type: bigquery.StringFieldType},
						{Name: "match", Required: false, Type: bigquery.StringFieldType},
						{Name: "parameters", Required: false, Type: bigquery.StringFieldType},
					},
				},
			},
		},
		{
			Name:        "feedMessage",
			Type:        bigquery.RecordFieldType,
			Description: "The message from Cloud Asset Inventory in realtime or from split dump in batch",
			Schema: bigquery.Schema{
				{
					Name: "asset",
					Type: bigquery.RecordFieldType,
					Schema: bigquery.Schema{
						{Name: "name", Required: true, Type: bigquery.StringFieldType},
						{Name: "owner", Required: false, Type: bigquery.StringFieldType},
						{Name: "violationResolver", Required: false, Type: bigquery.StringFieldType},
						{Name: "ancestryPathDisplayName", Required: false, Type: bigquery.StringFieldType},
						{Name: "ancestryPath", Required: false, Type: bigquery.StringFieldType},
						{Name: "ancestorsDisplayName", Required: false, Type: bigquery.StringFieldType},
						{Name: "ancestors", Required: false, Type: bigquery.StringFieldType},
						{Name: "assetType", Required: true, Type: bigquery.StringFieldType},
						{Name: "iamPolicy", Required: false, Type: bigquery.StringFieldType},
						{Name: "resource", Required: false, Type: bigquery.StringFieldType},
						{Name: "updateTime", Required: false, Type: bigquery.TimestampFieldType},
						{Name: "scheduledRootTriggeringID", Required: false, Type: bigquery.StringFieldType},
					},
				},
				{
					Name: "window",
					Type: bigquery.RecordFieldType,
					Schema: bigquery.Schema{
						{Name: "startTime", Required: true, Type: bigquery.TimestampFieldType},
					},
				},
				{Name: "origin", Required: false, Type: bigquery.StringFieldType},
			},
		},
		{Name: "regoModules", Required: false, Type: bigquery.StringFieldType, Description: "The rego code, including the rule template used to assess the rule as a JSON document"},
	}
	var violationBQ violationBQ

	violationBQ.NonCompliance.EvaluationTimeStamp = violation.NonCompliance.EvaluationTimeStamp
	violationBQ.NonCompliance.Message = violation.NonCompliance.Message
	violationBQ.NonCompliance.Metadata = string(violation.NonCompliance.Metadata)
	violationBQ.FunctionConfig = violation.FunctionConfig
	violationBQ.ConstraintConfig.Kind = violation.ConstraintConfig.Kind
	violationBQ.ConstraintConfig.Metadata.Name = violation.ConstraintConfig.Metadata.Name
	violationBQ.ConstraintConfig.Metadata.Annotations = string(violation.ConstraintConfig.Metadata.Annotations)
	violationBQ.ConstraintConfig.Spec.Severity = violation.ConstraintConfig.Spec.Severity
	violationBQ.ConstraintConfig.Spec.Match = string(violation.ConstraintConfig.Spec.Match)
	violationBQ.ConstraintConfig.Spec.Parameters = string(violation.ConstraintConfig.Spec.Parameters)
	violationBQ.FeedMessage.Window = violation.FeedMessage.Window
	violationBQ.FeedMessage.Origin = violation.FeedMessage.Origin
	violationBQ.FeedMessage.Asset.Name = violation.FeedMessage.Asset.Name
	violationBQ.FeedMessage.Asset.Owner = violation.FeedMessage.Asset.Owner
	violationBQ.FeedMessage.Asset.ViolationResolver = violation.FeedMessage.Asset.ViolationResolver
	violationBQ.FeedMessage.Asset.AssetType = violation.FeedMessage.Asset.AssetType
	violationBQ.FeedMessage.Asset.Ancestors = string(violation.FeedMessage.Asset.Ancestors)
	violationBQ.FeedMessage.Asset.AncestorsDisplayName = string(violation.FeedMessage.Asset.AncestorsDisplayName)
	violationBQ.FeedMessage.Asset.AncestryPath = violation.FeedMessage.Asset.AncestryPath
	violationBQ.FeedMessage.Asset.AncestryPathDisplayName = violation.FeedMessage.Asset.AncestryPathDisplayName
	violationBQ.FeedMessage.Asset.IamPolicy = string(violation.FeedMessage.Asset.IamPolicy)
	violationBQ.FeedMessage.Asset.Resource = string(violation.FeedMessage.Asset.Resource)
	violationBQ.FeedMessage.Asset.UpdateTime = violation.FeedMessage.Asset.UpdateTime
	violationBQ.RegoModules = string(violation.RegoModules)
	// log.Printf("%s", violationBQ.RegoModules)
	// log.Printf("violationBQ.FeedMessage.Asset.UpdateTime %v", violationBQ.FeedMessage.Asset.UpdateTime)

	if violationBQ.FeedMessage.Origin != "real-time" {
		violationBQ.FeedMessage.Asset.ScheduledRootTriggeringID = ev.StepStack[0].StepID
	}

	insertID := str.Hash(fmt.Sprintf("%s%v%s%v%v%s",
		violationBQ.FeedMessage.Asset.Name,
		violation.FeedMessage.Window.StartTime,
		violation.FunctionConfig.FunctionName,
		violation.FunctionConfig.DeploymentTime,
		violationBQ.NonCompliance.EvaluationTimeStamp,
		violation.NonCompliance.Message))

	savers := []*bigquery.StructSaver{
		{
			Struct:   violationBQ,
			Schema:   violationSchema,
			InsertID: insertID,
		},
	}
	err = inserter.Put(ctx, savers)
	return err
}
